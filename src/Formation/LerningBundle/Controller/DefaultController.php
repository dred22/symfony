<?php

namespace Formation\LerningBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
      $name = $this->get('oc_platform.testsevice')->getText();
        return $this->render('FormationLerningBundle:Default:index.html.twig', array('name' => $name));
    }
    
      public function menuAction()
      {
        // On fixe en dur une liste ici, bien entendu par la suite
        // on la récupérera depuis la BDD !
        $menuList = array('accueil','galerie','contact','newsletter','login');
    
        return $this->render('FormationLerningBundle:Default:menu.html.twig', array('menuList' =>$menuList));
      }
    public function contactAction()
    {
        return $this->render('FormationLerningBundle:Default:contact.html.twig');
    }
    public function galerieAction()
    {
      $imgList = glob('img/galerie/*.jpg');
      //$imgs = glob('*');
      //echo '<pre>';
      //var_dump($imgs);
      //die();
      return $this->render('FormationLerningBundle:Default:galerie.html.twig', array('imgList'=>$imgList));
    }
    public function newsletterAction()
    {
        return $this->render('FormationLerningBundle:Default:newsletter.html.twig');
    }
    public function loginAction()
    {
                $antispam = $this->container->get('oc_platform.antispam');
       
        /*$text = '...';
        if ($antispam->isSpam($text)) {
          throw new \Exception('Votre message a été détecté comme spam !');
        }*/
        return $this->render('FormationLerningBundle:Default:login.html.twig');
    }
}
