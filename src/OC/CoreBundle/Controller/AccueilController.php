<?php
namespace OC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccueilController extends Controller
{
  public function indexAction($limit)
  {
      /*echo "$limit";
      die();*/
      $em = $this->getDoctrine()->getManager();
      $repository = $em->getRepository('OCPlatformBundle:Advert');
      $listAdverts = $repository->findBy(array(),array('id' => 'DESC'),$limit);
      return $this->render('OCCoreBundle:Accueil:index.html.twig', array('listAdverts' => $listAdverts));
  }
}