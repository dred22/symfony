<?php
namespace OC\PlatformBundle\Services;

class ObjectToTable {
    function getTable($object){
        if(!is_object($object)){
            return false;
        }
        $table = [];
        foreach($object as $key=>$value ){
            $table[$key] = $value;
        }
        
        return $table ;
    }
}