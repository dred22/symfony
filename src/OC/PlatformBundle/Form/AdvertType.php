<?php

namespace OC\PlatformBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AdvertType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('date',      'date')
          ->add('title',     'text')
          ->add('author',    'text')
          ->add('content',   'textarea')
          ->add('image',      new ImageType(), array('required' => false))
          ->add('published', 'checkbox', array('required' => false))
          ->add('categories', 'collection', array(
            'type'         => new CategoryType(),
            'allow_add'    => true,
            'allow_delete' => true
          ))
          ->add('save',      'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'OC\PlatformBundle\Entity\Advert',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'oc_platformbundle_advert';
    }
}
